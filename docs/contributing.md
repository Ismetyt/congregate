# Contributing to Congregate

We love your input! We want to make contributing as easy and transparent as possible, whether it's:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing/Contributing new features

## We Develop with GitLab

We use GitLab to host this project. We use Gitlab to track issues and feature requests, as well as accept merge requests.

## We identify new work (e.g. bugs or features requests, etc.) using GitLab issues

We use this project's gitlab issues board to track public bugs or feature request issues. Do this by [opening a new issue](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate/-/issues).

## We collaborate on all code changes through merge requests

You can find a list of merge requests [here](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate/-/merge_requests).

To make your first merge request with a proposed change:

1. Edit the file(s) using the WebIDE, single file editor using the GitLaB UI, or using your local development environment of choice.
1. Once you've made the changes you're proposing, commit it to a new branch and open a merge request to merge it into main/master.
1. On the create MR page:
   - Add details about why you want to make the proposed change and a summary of what those changes are.
   - In the description field, include `closes #<issue-number>` to associate the MR to an issue. If you don't have an issue associated with this work, create one!

## Any contributions you make will be under the CCL Software License

In short, when you submit code changes, your submissions are understood to be under the same [CCL License](/LICENSE) that covers the project. Feel free to contact the maintainers if that's a concern.

## Troubleshooting support

Congregate is a [source available](https://about.gitlab.com/handbook/marketing/strategic-marketing/tiers/#open-source-vs-source-available) project released under the CCL License. If you have a paid GitLab subscription, please note that Congregate is not packaged as a part of GitLab, and falls outside of the scope of support. For more information, see GitLab's [Statement of Support](https://about.gitlab.com/support/statement-of-support.html).

Please fill out an issue in this project's issue tracker and the GitLab Professional Services Team will respond as soon as we are available to help you.
