# GitHub Migration features

:white_check_mark: = supported

:heavy_minus_sign: = not yet supported / in development

:x: = not supported

NOTE: Features not listed in the matrix should be presumed to be not yet supported.

| Main             | Feature                   | Sub-feature                                                      | GitLab             | Congregate         |
| ---------------- | ------------------------- | ---------------------------------------------------------------- | ------------------ | ------------------ |
| **Organization** |
|                  | Members                   |                                                                  | :x:                | :white_check_mark: |
|                  | Teams                     |                                                                  | :x:                | :white_check_mark: |
| **Repository**   |
|                  | Description               |                                                                  | :white_check_mark: | :white_check_mark: |
|                  | Git repository data       |                                                                  | :white_check_mark: | :white_check_mark: |
|                  | Issues                    |                                                                  | :white_check_mark: | :white_check_mark: |
|                  | Pull Requests             | Pull request comments, Review comments                           | :white_check_mark: | :white_check_mark: |
|                  | Members                   |                                                                  | :white_check_mark: | :white_check_mark: |
|                  | Wiki                      |                                                                  | :white_check_mark: | :white_check_mark: |
|                  | Milestones                |                                                                  | :white_check_mark: | :white_check_mark: |
|                  | Labels                    |                                                                  | :white_check_mark: | :white_check_mark: |
|                  | Release note descriptions |                                                                  | :white_check_mark: | :white_check_mark: |
|                  | LFS Objects               |                                                                  | :white_check_mark: | :white_check_mark: |
|                  | Members                   |                                                                  | :white_check_mark: | :white_check_mark: |
|                  | Branches                  |                                                                  | :white_check_mark: | :white_check_mark: |
|                  | Protected Branches        | Merge Access Levels, Push Access Levels, Unprotect Access Levels | :x:                | :white_check_mark: |
|                  | Protected Tags            | Create Access Levels                                             | :x:                | :white_check_mark: |
|                  | Push rules                |                                                                  | :x:                | :white_check_mark: |
|                  | Merge request approvals   |                                                                  | :x:                | :white_check_mark: |
|                  | Pages                     |                                                                  | :x:                | :heavy_minus_sign: |
|                  | Default Branch            |                                                                  | :x:                | :x:                |
|                  | Deploy keys               |                                                                  | :x:                | :x:                |
|                  | Webhooks (w/o token)      |                                                                  | :x:                | :x:                |
| **User**         |
|                  | Avatars                   |                                                                  | :x:                | :x:                |
|                  | Profile (API)             |                                                                  | :x:                | :x:                |
|                  | Preferences (API)         |                                                                  | :x:                | :x:                |
|                  | SSH keys                  |                                                                  | :x:                | :x:                |
|                  | GPG keys                  |                                                                  | :x:                | :x:                |
|                  | Secondary emails          |                                                                  | :x:                | :x:                |
|                  | Account settings          |                                                                  | :x:                | :x:                |
|                  | Access Tokens             |                                                                  | :x:                | :x:                |
